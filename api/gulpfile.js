var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    gulpMocha = require('gulp-mocha'),
    env = require('gulp-env');

gulp.task('default', ['common'], function () {
    nodemon({
        script: 'api.js',
        ext: 'js',
        env: {
            PORT: 3003
        },
        ignore: ['./node_modules/**']
    })
    .on('restart', function() {
        console.log('Restarting');
    });
});

gulp.task('common', function() {
    gulp.src('../common/**/*')
      .pipe(gulp.dest('./'));
});

gulp.task('test', function() {
    env({vars: {ENV:'Test'}});
    gulp.src('tests/*.js', {read: false})
        .pipe(gulpMocha({reporter: 'nyan'}))
});
