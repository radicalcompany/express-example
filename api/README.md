# Glenfiddich back-end
## Getting started
0. Install [Node.js](http://www.nodejs.org)
0. ```npm install -g nvm```
0. ```nvm install 6.9.1```
0. ```nvm use 6.9.1```
0. ```npm install -g gulp-cli```
0. ```npm install -g mocha```
0. ```npm install```
## Red, green, refactor
```gulp rgr```
