var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var mongoose = require('mongoose');
mongoose.connect('');
mongoose.Promise = global.Promise;

app.use(bodyParser.json({extended: false}));

app.use('/api/validateCode', require('./routes/card.routes.js')());
app.use('/api/image', require('./routes/image.routes.js')());
app.use('/api/venue', require('./routes/venue.routes.js')());
app.use('/api/validateVenue', require('./routes/venueCode.routes.js')());

var server = app.listen(3003);
