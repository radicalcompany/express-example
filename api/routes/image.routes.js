var express = require('express');
var imageRouter = express.Router();
var Image = require('../models/image.server.model.js');

var path = require('path');

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname, '../uploads'));
    },
    filename: function(req, file, cb) {
        cb(null, req.body.submissionId + '.' + file.mimetype.split('/').pop());
    }
});

var upload = multer({
    storage: storage
})

var router = function() {
    imageRouter.route('/')
        .post(upload.single('card'), function(req, res) {
            var image = new Image({
                submissionId: req.body.submissionId,
                submittedOn: Date.now(),
                type: req.file.mimetype,
                path: req.file.path
            });
            image.save(function(error, result) {
                res.status(200).end();
            });
        });
    return imageRouter;
};

module.exports = router;
