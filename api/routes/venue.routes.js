var express = require('express');
var venueRouter = express.Router();
var Venue = require('../models/venue.server.model.js');
var mailService = require('../services/emailService.js');

var router = function() {

    venueRouter.route('/').post(function(req, res) {

        var v = new Venue(req.body);

        console.log(v);

        // Rebuild the string to 5 digits with leading zeroes
        for (var i = 0; i < v.codes.length; i++) {
            while (v.codes[i].length < 5) {
                v.codes[i] = '0' + v.codes[i];
            }
        }

        v.submittedOn = Date.now();

        v.save(function(error, result) {
            mailService.sendMail(v.email, 'tradeSignUp');
            res.status(200).send(result).end();
        });
    });

    venueRouter.route('/find').post(function(req, res) {
        Venue.find({
            location: {
                $geoWithin: {
                    $box: [
                        [
                            req.body.north, req.body.east
                        ],
                        [req.body.south, req.body.west]
                    ]
                }
            }
        }, 'name email phone address location', function(error, results) {
            res.send(results).status(200);
            return;
        });
    });
    return venueRouter;
}

module.exports = router;
