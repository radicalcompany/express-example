var express = require('express');
var cardRouter = express.Router();
var Card = require('../models/card.server.model.js');
var Venue = require('../models/venue.server.model.js');

var router = function() {
    cardRouter.route('/').post(function(req, res) {

        if (typeof req.body.code === 'undefined') {
            res.status(400).send({error: 'No code was specified.'}).end();
            return;
        }

        // Remove leading zeroes from the input
        let batchNumber = req.body.code.replace(/^0+/, '');

        // Validate it's an integer
        if (batchNumber != parseInt(batchNumber, 10)) {
            res.status(400).send({error: 'Invalid format.'}).end();
            return;
        }

        // Rebuild the string to 5 digits with leading zeroes
        while (batchNumber.length < 5){
            batchNumber = '0' + batchNumber;
        }

        Card.findOne({
            batch: batchNumber
        }, function(err, card) {
            if (err) {
                res.status(500).end();
                return;
            }
            if (!card) {
                res.status(404).send({error: 'Code not found.'}).end();
                return;
            }
            Venue.count({
                codes: card.batch
            }, function(err, count) {
                if (count > 0) {
                    res.status(400).send({error: 'Code has already been registered.'}).end();
                    return;
                }
                res.status(200).end();
            });
        });
    });
    return cardRouter;
}

module.exports = router;
