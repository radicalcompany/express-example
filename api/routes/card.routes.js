var express = require('express');
var cardRouter = express.Router();

var router = function() {

    cardRouter.route('/').post(function(req, res) {

        if (typeof req.body.code === 'undefined') {
            res.status(400).send({
                error: 'No code was specified.'
            }).end();
            return;
        }

        if (req.body.code.length !== 8) {
            res.status(400).send({
                error: 'Code is in an invalid format.'
            }).end();
            return;
        }

        require('../controllers/card.server.controller.js')
            .get(req.body.code, function(err, result) {
                if (err) {
                    res.status(500).end();
                    return;
                }
                if (!result) {
                    res.status(404).send({
                        error: 'Code not found.'
                    }).end();
                    return;
                }
                if (result.redeemed) {
                    res.status(400).send({
                        error: 'Code has already been redeemed.'
                    }).end();
                    return;
                }
                res.status(200).end();
            });
    });

    return cardRouter;
}

module.exports = router;
