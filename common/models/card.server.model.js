var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var cardModel = new Schema({
    code: {type: String, index: true},
    batch: {type: String},
    highValue: {type: Boolean},
    redeemed: {type: Boolean},
});

module.exports = mongoose.model('Cards', cardModel);
