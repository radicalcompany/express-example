var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var imageModel = new Schema({
    submissionId: {type: String, index: true},
    submittedOn: {type:Date, index: true},
    type: {type: String},
    path: {type: String}
});

module.exports = mongoose.model('Images', imageModel);
