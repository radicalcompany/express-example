var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var sectionModel = new Schema({
    name: {type: String},
    code: {type: String, index: true},
    enabled: {type: Boolean, default:false}
});

module.exports = mongoose.model('Sections', sectionModel);
