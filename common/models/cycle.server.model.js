var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var cycleModel = new Schema({
    name: {type: String},
    start: {type: Date, index:true},
    end: {type: Date, index:true},
    type: {type: String},
    validated: {type: Boolean, default: false},
});

module.exports = mongoose.model('Cycles', cycleModel);
