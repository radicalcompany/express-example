var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var userModel = new Schema({
    name: {type: String, index: true},
    password: {type: String},
    type: {type: String}
});

module.exports = mongoose.model('Users', userModel);
