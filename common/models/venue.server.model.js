var mongoose = require('mongoose'),
    Schema = mongoose.Schema
    mongooseToCsv = require('mongoose-to-csv');


var collapseAddress = function(addr) {

    var result = '';

    for (var i = 0; i < addr.lines.length; i++) {
        if (addr.lines[i]) {
            result += addr.lines[i] + ' ';
        }
    }

    if (addr.town) {
        result += addr.town + ' ';
    }
    if (addr.county) {
        result += addr.county + ' ';
    }

    result += addr.postcode;

    return result;
};


var venueModel = new Schema({
    'name': {
        type: String
    },
    'email': {
        type: String
    },
    'phone': {
        type: String
    },
    'address': {
        lines: [
            {
                type: String
            }
        ],
        town: {
            type: String
        },
        county: {
            type: String
        },
        postcode: {
            type: String
        }
    },
    'codes': [
        {
            type: String
        }
    ],
    'marketing' : {type: Boolean},
    'registrantFirstName': {
        type: String
    },
    'registrantSecondName': {
        type: String
    },
    'submittedOn': {
        type: Date
    },
    location: {
        index: '2dsphere',
        type: [Number]
    }
});


venueModel.plugin(mongooseToCsv, {
    headers: 'Name Registrant eMail Phone Address Code Marketing SubmittedOn Location',
    constraints: {
        'Name'  : 'name',
        'eMail' : 'email',
        'Phone' : 'phone',
        'Marketing' : 'marketing',
        'SubmittedOn' : 'submittedOn'
    },
    virtuals: {
        'Address' : function(doc) {
            return collapseAddress(doc.address);
        },
        'Code' : function(doc) {
            return doc.codes.join(' ');
        },
        'Location' : function(doc) {
            return doc.location.join(' ');
        },
        'Registrant' : function(doc) {
            return doc.registrantFirstName + ' ' + doc.registrantSecondName;
        }
    }
});

venueModel.pre('save', function(next) {
    if (this.isNew && Array.isArray(this.location) && 0 === this.location.length) {
        this.location = undefined;
    }
    next();
})

module.exports = mongoose.model('Venue', venueModel);
