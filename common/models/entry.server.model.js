var mongoose = require('mongoose'),
    Schema = mongoose.Schema
    mongooseToCsv = require('mongoose-to-csv');

var collapseAddress = function(addr) {

    var result = '';

    for (var i = 0; i < addr.lines.length; i++) {
        if (addr.lines[i]) {
            result += addr.lines[i] + ' ';
        }
    }

    if (addr.town) {
        result += addr.town + ' ';
    }
    if (addr.county) {
        result += addr.county + ' ';
    }

    result += addr.postcode;

    return result;
};

var decodeWinType = function(winType) {

    switch (winType) {
        case 'glasses':
            return 'Glasses';
            break;
        case 'rld':
            return 'Red Letter Day Experience';
            break;
        case 'holiday':
            return 'NYC Holiday';
            break;
        default:
            return 'No Win';
    }
}

var entryModel = new Schema({
    'name' : {type: String, index: true},
    'email' : {type: String, index: true},
    'mobile' : {type: String},
    'address': {
        lines: [{type: String}],
        town: {type: String},
        county: {type: String},
        postcode: {type: String}
    },
    'code' : {type: String, index: true},
    'wintype' : {type: String},
    'ticket' : {type:String},
    'marketing' : {type: Boolean},
    'submittedOn' : {type:Date, index: true},
    'batchId' : {type:String},
    'validated' : {type:Boolean, index: true},
    'valid' : {type:Boolean},
    'validatedBy' : {type:String},
    'validatedOn' : {type:Date}
});


entryModel.plugin(mongooseToCsv, {
    headers: 'Name eMail Mobile Address Code BatchId WinType Marketing SubmittedOn Validated Valid ValidatedOn ValidatedBy',
    constraints: {
        'Name'  : 'name',
        'eMail' : 'email',
        'Mobile' : 'mobile',
        'Code' : 'code',
        'BatchId' : 'batchId',
        'Marketing' : 'marketing',
        'SubmittedOn' : 'submittedOn',
        'Validated' : 'validated',
        'Valid' : 'valid',
        'ValidatedOn' : 'validatedOn',
        'ValidatedBy' : 'validatedBy'
    },
    virtuals: {
        'Address' : function(doc) {
            return collapseAddress(doc.address);
        },
        'WinType' : function(doc) {
            return decodeWinType(doc.wintype);
        }
    }
});

module.exports = mongoose.model('Entries', entryModel);
