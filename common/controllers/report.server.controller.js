const async = require('async');
const moment = require('moment');
const Venue = require('../models/venue.server.model.js');
const Entry = require('../models/entry.server.model.js');
const Image = require('../models/image.server.model.js');

module.exports.get = function(start, finish, callback) {

    const begin = moment(start, 'DD/MM/YYYY').toDate();
    const end = moment(finish, 'DD/MM/YYYY').add(1, 'days').toDate();

    async.parallel({

        totalVenueCount: function(cb) {
            Venue.count(cb);
        },

        venueCount: function(cb) {
            Venue.count({'submittedOn' : {
                $gte: begin,
                $lt: end
            }}, cb);
        },

        totalEntryCount: function(cb) {
            Entry.count(cb);
        },

        entryCount: function(cb) {
            Entry.count({'submittedOn' : {
                $gte: begin,
                $lt: end
            }}, cb);
        },

        totalImageCount: function(cb) {
            Image.count(cb);
        },

        imageCount: function(cb) {
            Image.count({'submittedOn' : {
                $gte: begin,
                $lt: end
            }}, cb);
        },

    }, callback);
};
