var Section = require('../models/section.server.model.js');

module.exports.get = function(queryName, cb) {
    Section.findOne({
        'code': queryName
    }, function(err, res) {
        if (err) {
            cb(err, null);
        } else {
            cb(null, {enabled: res.enabled});
        }
    });
};
