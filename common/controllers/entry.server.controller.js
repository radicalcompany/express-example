var Entry = require('../models/entry.server.model.js');
var mailService = require('../services/emailService.js');

var decodeWinType = function(winType) {

    switch (winType) {
        case 'glasses':
            return 'Glasses';
            break;
        case 'rld':
            return 'Red Letter Day Experience';
            break;
        case 'holiday':
            return 'NYC Holiday';
            break;
        default:
            return 'No Win';
    }
}

var entryController = function() {

    var get = function(cb) {
        find({}, cb);
    }

    var find = function(searchParams, cb) {

        var search = {};

        if(searchParams.code){
            search['code'] = searchParams.code;
        }

        if(searchParams.name){
            search['name'] = { "$regex": searchParams.name, "$options": "i" };
        }

        if(searchParams.email){
            search['email'] = searchParams.email;
        }

        Entry.find(search, function(err, results) {

            if(err){
                cb(err, null);
                return;
            }

            let result = {
                open: [],
                closed: [],
                search: searchParams,
                searched: searchParams.name || searchParams.code || searchParams.email
            };

            for (var i = 0; i < results.length; i++) {
                if (results[i].validated) {
                    result.closed.push({
                        id: results[i]._id,
                        name: results[i].name,
                        code: results[i].code,
                        prize: decodeWinType(results[i].wintype),
                        submittedOn: results[i].submittedOn,
                        validatedBy: results[i].validatedBy,
                        validatedOn: results[i].validatedOn,
                        isValid: results[i].valid
                            ? 'Yes'
                            : 'No'
                    });

                } else {
                    result.open.push({
                        id: results[i]._id,
                        name: results[i].name,
                        code: results[i].code,
                        prize: decodeWinType(results[i].wintype),
                        submittedOn: results[i].submittedOn
                    });
                }
            }
            cb(null, result);
        });
    }

    var save = function(req, res, card) {
        var entry = new Entry(req.body);

        var cutoff = new Date();
        cutoff.setDate(cutoff.getDate() - 1);

        Entry.where({
            email: entry.email,
            submittedOn: {
                $gt: cutoff
            }
        }).count(function(err, result) {
            if (result > 1) {
                res.status(400).send({error: 'Already made two submissions today.'}).end();
                return;
            }

            entry.submittedOn = Date.now();
            entry.batchId = card.batch;

            if (entry.wintype == 'none') {
                entry.valid = true;
                entry.validated = true;
                entry.validateOn = Date.now();
                entry.validatedBy = 'System';
            }

            entry.save(function(err, result) {

                if (card) {
                    card.redeemed = true;
                    card.save();
                }

                // send the email to the user
                if(entry.wintype === 'none'){
                    mailService.sendMail(entry.email, 'consumerNoWin');
                }else {
                    mailService.sendMail(entry.email, 'consumerWin');
                }

                res.status(200).send({ticket: result._id}).end();
                return;
            });
        });
    }

    return {save: save, find: find, get: get}
}

module.exports = entryController;
