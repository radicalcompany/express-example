var Cycle = require('../models/cycle.server.model.js');


module.exports.getActive = function(cb) {
    Cycle.find({
        'start': {
            '$lte': new Date(Date.now())
        },
        'end': {
            '$gte': new Date(Date.now())
        }
    }, function(error, result) {
        if (error) {
            cb(error, null);
            return;
        }
        if (result.length !== 2) {
            cb({error: 'Invalid Cycle Configuration'}, null);
            return;
        }

        var shapedResults = {};

        for (var i = 0; i < result.length; i++) {
            if (result[i].type == 'Monthly') {
                shapedResults.Monthly = result[i];
            }
            if (result[i].type === 'Quarterly') {
                shapedResults.Quarterly = result[i];
            }
        }

        cb(null, shapedResults);
    });
};
