var request = require('request');
var ejs = require('ejs');
var fs = require('fs');
var path = require('path');

const apiKey = '';
const apiAddress = '';
const sourceAddress = '';
const baseUrl = '';

const mailTypes = [
    {
        type: 'consumerWin',
        subject: 'THANKS FOR ENTERING YOUR DETAILS',
        footer: 'consumerFooter',
        link: 'www.test.co.uk'
    }
]

module.exports.sendMail = function(sendTo, mailType, additionalData) {

    for (let i = 0; i < mailTypes.length; i++) {
        if (mailType === mailTypes[i].type) {

            var vm = {
                subject: mailTypes[i].subject,
                footer: mailTypes[i].footer,
                body: `${mailTypes[i].type}.ejs`,
                link: mailTypes[i].link,
                base: baseUrl
            };

            if (additionalData) {
                vm.model = additionalData;
            }

            ejs.renderFile(path.join(__dirname, '../email/content/template.ejs'), vm, {}, function(err, result) {

                const options = {
                    method: 'POST',
                    url: apiAddress,
                    headers: {
                        'cache-control': 'no-cache',
                        'authorization': 'Basic ' + new Buffer('api:' + apiKey).toString('base64')
                    },
                    formData: {
                        from: 'test@test.com',
                        to: sendTo,
                        subject: vm.subject,
                        html: result
                    }
                };

                request(options, function(error, response, body) {
                    console.log(body);
                });
            });
        }
    }

};
